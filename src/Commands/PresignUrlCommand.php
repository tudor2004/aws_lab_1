<?php

namespace AwsTraining\TestOne\Commands;

use Aws\S3\MultipartUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Aws\S3\S3Client;

class PresignUrlCommand extends Command
{
	protected function configure()
	{
		$this
			->setName('aws:pre-signed-url')
			->setDescription('Create pre-signed URL.')
			->addOption('bucket', 'b', InputOption::VALUE_REQUIRED)
			->addOption('key', 'k', InputOption::VALUE_REQUIRED)
			->addOption('region', 'r', InputOption::VALUE_OPTIONAL, 'Region name', 'us-east-1');

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $input->getOption('region'),
			"credentials" => [
				'key'    => getenv('AWS_KEY_ID'),
				'secret' => getenv('AWS_SECRET_KEY'),
			],
		]);

		$cmd = $s3->getCommand('GetObject', [
			'Bucket' => $input->getOption('bucket'),
			'Key' => $input->getOption('key')
		]);

		$request = $s3->createPresignedRequest($cmd, '+20 minutes');


		$presignedUrl = (string)$request->getUri();

		$output->writeln('Pre-signed URL: ' . $presignedUrl);

	}
}