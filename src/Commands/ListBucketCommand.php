<?php

namespace AwsTraining\TestOne\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Aws\S3\S3Client;

class ListBucketCommand extends Command
{
	protected function configure()
	{
		$this
			->setName('aws:list-bucket')
			->setDescription('List bucket content.')
			->addOption('bucket', 'b', InputOption::VALUE_REQUIRED)
			->addOption('region', 'r', InputOption::VALUE_OPTIONAL, 'Region name', 'us-east-1');

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $input->getOption('region'),
			"credentials" => [
				'key'    => getenv('AWS_KEY_ID'),
				'secret' => getenv('AWS_SECRET_KEY'),
			],
		]);

		$result = $s3->listObjects([
			'Bucket' => $input->getOption('bucket'),
		]);

		$table = new Table($output);

		$table->setHeaders(['#', 'Name', 'Size', 'Last Modified']);

		$idx = 0;

		foreach($result['Contents'] as $key) {
			$table->addRow([
				++$idx,
				$key['Key'],
				$key['Size'],
				$key['LastModified']
			]);
		}

		$table->render();
	}
}