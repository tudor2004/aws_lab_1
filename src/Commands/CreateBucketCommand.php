<?php

namespace AwsTraining\TestOne\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Aws\S3\S3Client;

class CreateBucketCommand extends Command
{
	protected function configure()
	{
		$this
			->setName('aws:create-bucket')
			->setDescription('Create a new bucket.')
			->addOption('bucket', 'b', InputOption::VALUE_REQUIRED)
			->addOption('region', 'r', InputOption::VALUE_OPTIONAL, 'Region name', 'us-east-1');

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $input->getOption('region'),
			"credentials" => [
				'key'    => getenv('AWS_KEY_ID'),
				'secret' => getenv('AWS_SECRET_KEY'),
			],
		]);

		$result = $s3->createBucket([
			'Bucket' => $input->getOption('bucket'),
		]);

		$output->write(var_export($result, true));
	}
}