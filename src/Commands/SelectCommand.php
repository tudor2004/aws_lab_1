<?php

namespace AwsTraining\TestOne\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Aws\S3\S3Client;

class SelectCommand extends Command
{
	protected function configure()
	{
		$this
			->setName('aws:select')
			->setDescription('Get from S3 key using select.')
			->addOption('bucket', 'b', InputOption::VALUE_REQUIRED)
			->addOption('key', 'k', InputOption::VALUE_REQUIRED)
			->addOption('region', 'r', InputOption::VALUE_OPTIONAL, 'Region name', 'us-east-1');

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $input->getOption('region'),
			"credentials" => [
				'key'    => getenv('AWS_KEY_ID'),
				'secret' => getenv('AWS_SECRET_KEY'),
			],
		]);

		$result = $s3->selectObjectContent([
			'Bucket'              => $input->getOption('bucket'),
			'Key'                 => $input->getOption('key'),
			'ExpressionType'      => 'SQL',
			'Expression'          => 'SELECT * FROM S3Object WHERE cast(age AS int) > 3',
			'InputSerialization'  => [
				'CSV' => [
					'FileHeaderInfo'  => 'USE',
					//'RecordDelimiter' => '\n',
					'FieldDelimiter'  => ',',
				],
			],
			'OutputSerialization' => [
				'CSV' => [],
			],
		]);

		foreach ($result['Payload'] as $event) {
			if (isset($event['Records'])) {
				echo (string) $event['Records']['Payload'] . PHP_EOL;
			} elseif (isset($event['Stats'])) {
				echo 'Processed ' . $event['Stats']['Details']['BytesProcessed'] . ' bytes' . PHP_EOL;
			} elseif (isset($event['End'])) {
				echo 'Complete.' . PHP_EOL;
			}
		}


	}
}