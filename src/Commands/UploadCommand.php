<?php

namespace AwsTraining\TestOne\Commands;

use Aws\S3\MultipartUploader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Aws\S3\S3Client;

class UploadCommand extends Command
{
	protected function configure()
	{
		$this
			->setName('aws:upload')
			->setDescription('Upload file to bucket.')
			->addOption('bucket', 'b', InputOption::VALUE_REQUIRED)
			->addOption('file', 'f', InputOption::VALUE_REQUIRED)
			->addOption('region', 'r', InputOption::VALUE_OPTIONAL, 'Region name', 'us-east-1');

	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$s3 = new S3Client([
			'version'     => 'latest',
			'region'      => $input->getOption('region'),
			"credentials" => [
				'key'    => getenv('AWS_KEY_ID'),
				'secret' => getenv('AWS_SECRET_KEY'),
			],
		]);


		$uploader = new MultipartUploader($s3, $input->getOption('file'), [
			'bucket'        => $input->getOption('bucket'),
			'key'           => 'uploads/' . basename($input->getOption('file')),
			'before_upload' => function (\Aws\Command $command) use ($output) {
				$output->writeln('Uploading part...');
			},
		]);


		try {
			$result = $uploader->upload();

			$output->writeln('Upload was complete' . var_export($result, true));
		} catch (\Exception $ex) {
			$output->writeln('<error>' . $ex->getMessage() . '</error>');
		}

	}
}